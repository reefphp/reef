<?php

namespace Reef\Layout\bootstrap5;

class bootstrap5 extends \Reef\Layout\Layout {
	
	private $a_config;
	
	public function __construct($a_config = []) {
		$this->a_config = array_merge([
			'break' => ['md' => 3],
		], $a_config);
	}
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'bootstrap5';
	}
	
	/**
	 * @inherit
	 */
	public static function getDir() : string {
		return __DIR__.'/';
	}
	
	/**
	 * @inherit
	 */
	public function getConfig() : array {
		return $this->a_config;
	}
	
	/**
	 * @inherit
	 */
	public function view(array $a_config = []) : array {
		$a_config = array_merge($this->a_config, $a_config);
		
		$a_break = $a_config['break'];
		
		$i_previous = 12;
		$a_bps_left = ['col-12'];
		$a_bps_right = ['col-12'];
		
		foreach(['xs', 'sm', 'md', 'lg', 'xl'] as $s_bp) {
			if(!isset($a_break[$s_bp]) || $a_break[$s_bp] == $i_previous) {
				continue;
			}
			
			$i_left = (int)$a_break[$s_bp];
			
			$a_bps_left[] = 'col-'.$s_bp.'-'.$i_left;
			$a_bps_right[] = 'col-'.$s_bp.'-'.(12-$i_left);
			
			$i_previous = $i_left;
		}
		
		return [
			'col_left' => implode(' ', $a_bps_left),
			'col_right' => implode(' ', $a_bps_right),
		];
	}
	
	/**
	 * @inherit
	 */
	public function getJS() : array {
		return [
			[
				'name' => 'bootstrap5',
				'type' => 'remote',
				'path' => 'https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js',
				'integrity' => 'sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4',
			],
		];
	}
	
	/**
	 * @inherit
	 */
	public function getCSS() : array {
		return [
			[
				'name' => 'bootstrap5',
				'type' => 'remote',
				'path' => 'https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css',
				'integrity' => 'sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65',
			],
		];
	}
}
