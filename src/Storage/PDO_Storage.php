<?php

namespace Reef\Storage;

use \PDO;
use \Reef\Exception\StorageException;

/**
 * PDO_Storage can be used to let Reef store data in a database using PDO
 */
abstract class PDO_Storage implements Storage {
    /**
     * The PDO_StorageFactory this object belongs to
     * @type PDO_StorageFactory
     */
    protected $StorageFactory;

    /**
     * The PDO object
     * @type PDO
     */
    protected $PDO;

    /**
     * Constructor
     * @param PDO_StorageFactory $StorageFactory The PDO_StorageFactory this object belongs to
     * @param PDO $PDO The PDO object
     * @throws StorageException If the table name does not exist
     */
    public function __construct(PDO_StorageFactory $StorageFactory, PDO $PDO) {
        $this->StorageFactory = $StorageFactory;
        $this->PDO = $PDO;
    }

    /**
     * Get the used PDO object
     * @return PDO
     */
    public function getPDO() {
        return $this->PDO;
    }

    /**
     * Start a transaction
     * @param PDO $PDO The PDO object
     */
    abstract public static function startTransaction(\PDO $PDO);

    /**
     * Add new savepoint to the current transaction
     * @param PDO $PDO The PDO object
     * @param string $s_savepoint The savepoint name
     */
    abstract public static function newSavepoint(\PDO $PDO, string $s_savepoint);

    /**
     * Rollback to a previously set savepoint
     * @param PDO $PDO The PDO object
     * @param string $s_savepoint The savepoint name to rollback to
     */
    abstract public static function rollbackToSavepoint(\PDO $PDO, string $s_savepoint);

    /**
     * Commit the current transaction
     * @param PDO $PDO The PDO object
     */
    abstract public static function commitTransaction(\PDO $PDO);

    /**
     * Rollback the current transaction
     * @param PDO $PDO The PDO object
     */
    abstract public static function rollbackTransaction(\PDO $PDO);

    /**
     * Add columns to the storage table
     * @param array $a_subfields Array of column structure arrays, indexed by new column name
     */
    abstract public function addColumns($a_subfields);

    /**
     * Remove columns from the storage table
     * @param array $a_subfields Array of update arrays, indexed by old column name. An update array consists of:
     *  - 'name'          => new column name
     *  - 'structureFrom' => old column structure array
     *  - 'structureTo'   => new column structure array
     */
    abstract public function updateColumns($a_subfields);

    /**
     * Remove columns from the storage table
     * @param string[] $a_columns The old column names to delete
     */
    abstract public function removeColumns($a_columns);

    /**
     * Get a list of column names
     * @return string[] The column names
     */
    abstract public function getColumns() : array;

    /**
     * Get the used table name
     * @return string
     */
    abstract public function getTableName();

    /**
     * Sanitize a name for use in a query, removing everything but alphanumeric characters and underscores.
     * @param string $s_name The name to sanitize
     * @return string
     */
    public static function sanitizeName($s_name) {
        return preg_replace('/[^a-zA-Z0-9_]/', '', $s_name);
    }

    /**
     * Quote and sanitize an identifier, such as a table name or column name
     * @param string $s_name The name to quote
     * @return string
     */
    abstract public static function quoteIdentifier($s_name);
}
