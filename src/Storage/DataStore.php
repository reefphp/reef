<?php

namespace Reef\Storage;

use \Reef\Reef;
use \Reef\Filesystem\Filesystem;

/**
 * The data store uses the StorageFactory passed in the Reef setup to store
 * data in the database. The data store keeps track of the form and submission
 * storages created, and allows to fetch and modify them.
 */
class DataStore {
	
	/**
	 * The Reef object this DataStore belongs to
	 * @type Reef
	 */
	private $Reef;
	
	/**
	 * The StorageFactory in use by this DataStore
	 * @type StorageFactory
	 */
	private $StorageFactory;

	/**
	 * Constructor
	 * @param Reef $Reef The Reef object
	 */
	public function __construct(Reef $Reef) {
		$this->Reef = $Reef;
		$this->StorageFactory = $this->Reef->getSetup()->getStorageFactory();
	}

	/**
	 * Utility function for applying modifications in the database and filesystem,
	 * allowing them to be rolled back when an error occurs.
	 * @param callable $fn_callback Callback performing the desired modifications
	 * @return mixed The result of $fn_callback
	 */
	public function ensureTransaction($fn_callback) {
		$Filesystem = $this->getFilesystem();
		$b_transaction = !$Filesystem->inTransaction();
		
		if($b_transaction) {
			$Filesystem->startTransaction();
		}
		
		try {
			$m_return = $this->StorageFactory->ensureTransaction($fn_callback);
			
			if($b_transaction) {
				$Filesystem->commitTransaction();
			}
		}
		catch(\Exception $e) {
			$Filesystem->rollbackTransaction();
			throw $e;
		}
		
		return $m_return;
	}
	
	/**
	 * Get the Filesystem object
	 * @return Filesystem
	 */
	public function getFilesystem() {
		return $this->Reef->getSetup()->getFilesystem();
	}
	
}
