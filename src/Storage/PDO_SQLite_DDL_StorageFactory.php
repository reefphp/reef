<?php

namespace Reef\Storage;

/**
 * Factory for PDO_SQLite_DDL_Storage objects
 */
class PDO_SQLite_DDL_StorageFactory extends PDO_DDL_StorageFactory {
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'pdo_sqlite_ddl';
	}
	
	/**
	 * @inherit
	 */
	protected function getStorageClass() {
		return '\\Reef\\Storage\\PDO_SQLite_DDL_Storage';
	}
	
	/**
	 * @inherit
	 */
	protected function getPDODriverName() {
		return 'sqlite';
	}
	
}
