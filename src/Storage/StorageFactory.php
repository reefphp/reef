<?php

namespace Reef\Storage;

use Reef\Form\AbstractStorableForm;
use Reef\Form\StoredForm;
use Reef\Reef;

/**
 * Interface for storageFactory classes
 */
interface StorageFactory {
	
	/**
	 * Return the identifying name of this storage
	 */
	public static function getName() : string;

    /**
     * Set the Reef object
     * @param Reef $Reef The Reef we are currently working in
     */
    public function setReef(Reef $Reef): void;

    /**
     * Retrieve the Form Storage object. If it does not exist yet, the storage may be created
     */
    public function getFormStorage(): Storage;

    /**
     * Determine whether a submission storage exists for the given form
     */
    public function hasSubmissionStorage(AbstractStorableForm $Form): bool;

    /**
     * Create a new Submission Storage
     */
    public function createSubmissionStorage(StoredForm $Form): Storage;

    /**
     * Retrieve a Submission Storage object
     */
    public function getSubmissionStorage(StoredForm $Form): Storage;

    /**
     * Delete a Submission Storage
     */
    public function deleteSubmissionStorage(StoredForm $Form): void;

    /**
     * Delete a Submission Storage if it exists; does not throw a StorageException if the storage does not exist
     */
    public function deleteSubmissionStorageIfExists(StoredForm $Form): void;
}
