<?php

namespace Reef\Storage;

use \PDO;
use \Reef\Exception\StorageException;
use \Reef\Exception\InvalidArgumentException;
use Reef\Form\AbstractStorableForm;
use Reef\Form\StoredForm;
use Reef\Reef;

/**
 * Factory for PDO_DDL_Storage objects
 */
abstract class PDO_DDL_StorageFactory extends PDO_StorageFactory {
    /**
     * The form storage instance
     * @type Storage
     */
    private $FormStorage;

    /**
     * The submission storage instances, indexed by storage name
     * @type Storage[]
     */
    private $a_submissionStorages = [];

    /**
     * The database prefix to use
     * @type string
     */
    private $s_prefix;

    /**
	 * Create a PDO factory using the specified PDO object
	 * @param PDO $PDO The PDO instance to use
	 * @return self
	 * @throws InvalidArgumentException If the used PDO driver is not supported
	 */
	public static function createDdlFactory(PDO $PDO) : PDO_DDL_StorageFactory {
		switch($PDO->getAttribute(PDO::ATTR_DRIVER_NAME)) {
			case 'sqlite':
				return new PDO_SQLite_DDL_StorageFactory($PDO);
			case 'mysql':
				return new PDO_MySQL_DDL_StorageFactory($PDO);
			// @codeCoverageIgnoreStart
			default:
				throw new InvalidArgumentException("Unsupported PDO driver ".$PDO->getAttribute(PDO::ATTR_DRIVER_NAME).".");
			// @codeCoverageIgnoreEnd
		}
	}

    public function setReef(Reef $Reef): void
    {
        parent::setReef($Reef);
        $this->s_prefix = $this->Reef->getOption('db_prefix');
    }

    /**
	 * @inherit
	 */
	public function getStorage(string $s_tableName) {
		$s_storageClass = $this->getStorageClass();
		
		return new $s_storageClass($this, $this->getPDO(), $s_tableName);
	}
	
	/**
	 * @inherit
	 */
	public function newStorage(string $s_tableName) {
        /** @var class-string<PDO_DDL_Storage> $s_storageClass */
		$s_storageClass = $this->getStorageClass();
		
		if($this->hasStorage($s_tableName)) {
			throw new StorageException("Storage ".$s_tableName." already exists");
		}
		
		$s_storageClass::createStorage($this, $this->getPDO(), $s_tableName);
		
		return $this->getStorage($s_tableName);
	}
	
	/**
	 * @inherit
	 */
	public function hasStorage(string $s_tableName) {
        /** @var class-string<PDO_DDL_Storage> $s_storageClass */
		$s_storageClass = $this->getStorageClass();
		
		return $s_storageClass::table_exists($this, $this->getPDO(), $s_tableName);
	}

    /**
     * @inherit
     */
    public function getFormStorage() : Storage {
        if(empty($this->FormStorage)) {
            if($this->hasStorage($this->s_prefix.'_forms')) {
                $this->FormStorage = $this->getStorage($this->s_prefix.'_forms');
            }
            else {
                $this->FormStorage = $this->newStorage($this->s_prefix.'_forms');

                $this->FormStorage->addColumns([
                    'definition' => [
                        'type' => Storage::TYPE_TEXT,
                        'limit' => 4194303,
                    ],
                ]);
            }
        }

        return $this->FormStorage;
    }

    /**
     * @inherit
     */
    public function hasSubmissionStorage(AbstractStorableForm $Form) : bool {
        return $this->hasStorage($this->s_prefix.$Form->getStorageName());
    }

    /**
     * @inherit
     */
    public function createSubmissionStorage(StoredForm $Form) : Storage {
        $s_storageName = $Form->getStorageName();

        if($s_storageName === null) {
            throw new StorageException('Storage name is not set');
        }

        if(!empty($this->a_submissionStorages[$s_storageName]) || $this->hasStorage($this->s_prefix.$s_storageName)) {
            throw new StorageException('Storage already exists');
        }

        $this->newStorage($this->s_prefix.$s_storageName);

        return $this->getSubmissionStorage($Form);
    }

    /**
     * @inherit
     */
    public function getSubmissionStorage(StoredForm $Form) : Storage {
        $s_storageName = $Form->getStorageName();

        if($s_storageName === null) {
            throw new StorageException('Storage name is not set');
        }

        if(empty($this->a_submissionStorages[$s_storageName])) {
            $this->a_submissionStorages[$s_storageName] = $this->getStorage($this->s_prefix.$s_storageName);
        }

        return $this->a_submissionStorages[$s_storageName];
    }

    /**
     * @inherit
     */
    public function deleteSubmissionStorage(StoredForm $Form): void {
        $this->getSubmissionStorage($Form)->deleteStorage();
        unset($this->a_submissionStorages[$Form->getStorageName()]);
    }

    /**
     * @inherit
     */
    public function deleteSubmissionStorageIfExists(StoredForm $Form): void {
        if($this->hasSubmissionStorage($Form)) {
            $this->deleteSubmissionStorage($Form);
        }
    }

    /**
     * @inherit
     */
    public function changeSubmissionStorageName(StoredForm $Form, $s_newStorageName) {
        if(!$this->hasSubmissionStorage($Form)) {
            throw new StorageException("Storage not found for renaming");
        }

        $this->getSubmissionStorage($Form)->renameStorage(
            $this->s_prefix.$s_newStorageName
        );

        unset($this->a_submissionStorages[$Form->getStorageName()]);
    }
}
