<?php

namespace Reef\Storage;

use \PDO;
use Reef\Exception\LogicException;
use Reef\Exception\OutOfBoundsException;
use Reef\Exception\RuntimeException;
use \Reef\Exception\StorageException;

/**
 * Storage implementation using MySQL
 */
class PDO_MySQL_Json_Storage extends PDO_Storage {

    /**
     * The table name used by this storage object
     * @type string
     */
    protected $s_table;

    /**
     * The escaped table name
     * @type string
     */
    protected $qs_table;

    /**
     * Additional scoping properties
     * @var array
     */
    private $scope;

    public function __construct(PDO_StorageFactory $StorageFactory, PDO $PDO, string $s_table, array $scope = [])
    {
        parent::__construct($StorageFactory, $PDO);
        $this->s_table = $s_table;
        $this->qs_table = static::quoteIdentifier($this->s_table);
        $this->scope = $scope;

        foreach ($scope as $value) {
            if (!is_int($value) && !(is_string($value) && ctype_digit($value))) {
                throw new OutOfBoundsException('Only integer scopes are supported supported');
            }
        }
    }

    /**
	 * @inherit
	 */
	public static function startTransaction(\PDO $PDO) {
		$PDO->exec("START TRANSACTION;");
	}
	
	/**
	 * @inherit
	 */
	public static function newSavepoint(\PDO $PDO, string $s_savepoint) {
		$PDO->exec("SAVEPOINT ".static::quoteIdentifier($s_savepoint)." ;");
	}
	
	/**
	 * @inherit
	 */
	public static function rollbackToSavepoint(\PDO $PDO, string $s_savepoint) {
		$PDO->exec("ROLLBACK TO SAVEPOINT ".static::quoteIdentifier($s_savepoint).";");
	}
	
	/**
	 * @inherit
	 */
	public static function commitTransaction(\PDO $PDO) {
		$PDO->exec("COMMIT;");
	}
	
	/**
	 * @inherit
	 */
	public static function rollbackTransaction(\PDO $PDO) {
		$PDO->exec("ROLLBACK;");
	}
	
	/**
	 * Convert a column structure array into type converter SQL
	 * @param array $a_subfield The column structure array
	 * @return string The type converter SQL
	 */
	private function getSubfieldConverter(array $a_subfield): string
    {
		switch($a_subfield['type']) {
			case Storage::TYPE_TEXT:
				$i_limit = (int)($a_subfield['limit']??1000);
				$i_limit = max($i_limit, 1);

                $newValue = 'SUBSTR(CAST(%s AS CHAR), 1, ' . $i_limit . ')';

				if(isset($a_subfield['default'])) {
                    $newValue = 'COALESCE(' . $newValue . ', '.$this->PDO->quote($a_subfield['default']).')';
				}

                return $newValue;

			case Storage::TYPE_BOOLEAN:
                $newValue = 'IF(%s, true, false)';
				
				if(isset($a_subfield['default'])) {
                    $newValue = 'COALESCE(' . $newValue . ', ' . ($a_subfield['default'] > 0 ? 'true' : 'false') . ')';
				}

                return $newValue;
			
			case Storage::TYPE_INTEGER:
                $unsigned = isset($a_subfield['min']) && $a_subfield['min'] >= 0;

                $newValue = 'CAST(%s AS ' . ($unsigned ? 'UNSIGNED' : 'SIGNED') . ')';

                if (isset($a_subfield['min'])) {
                    $newValue = 'GREATEST(' . ((int)$a_subfield['min']) . ', ' . $newValue . ')';
                }
                if (isset($a_subfield['max'])) {
                    $newValue = 'LEAST(' . ((int)$a_subfield['max']) . ', ' . $newValue . ')';
                }

				if(isset($a_subfield['default'])) {
                    $newValue = 'COALESCE(' . $newValue . ', ' . ((int)$a_subfield['default']) . ')';
				}

                return $newValue;

			case Storage::TYPE_FLOAT:
                $newValue = 'CAST(%s AS REAL)';
				
				if(isset($a_subfield['default'])) {
                    $newValue = 'COALESCE(' . $newValue . ', ' . ((float)$a_subfield['default']) . ')';
				}

                return $newValue;
		}
		
		throw new \LogicException();
	}
	
	/**
	 * @inherit
	 */
	public function addColumns($a_subfields) {
        if (count($a_subfields) === 0) {
            return;
        }

        $s_query = "UPDATE ".$this->qs_table." SET data = JSON_SET(data";
		foreach($a_subfields as $s_column => $a_subfield) {
            $ss_column = static::sanitizeName($s_column);

            $newValue = sprintf($this->getSubfieldConverter($a_subfield), "JSON_UNQUOTE(JSON_EXTRACT(data, '$." . $ss_column . "'))");

            $s_query .= ", '$." . $ss_column . "', " . $newValue . " ";
        }

        $s_query .= ")" . $this->scopeWhere();

        $sth = $this->PDO->prepare($s_query);
        $sth->execute();

        if($sth->errorCode() !== '00000') {
            // @codeCoverageIgnoreStart
            throw new StorageException("Could not add JSON columns.");
            // @codeCoverageIgnoreEnd
        }
	}
	
	/**
	 * @inherit
	 */
	public function updateColumns($a_subfields) {
        if (count($a_subfields) === 0) {
            return;
        }

		$s_setExpr = "JSON_SET(data";

        $a_remove = [];
		foreach($a_subfields as $s_columnOld => $a_fieldUpdate) {
            $s_columnNew = $a_fieldUpdate['name'];

            if ($s_columnNew !== $s_columnOld) {
                $a_remove[] = $s_columnOld;
            }

            $oldValue = " IF(JSON_TYPE(JSON_EXTRACT(data, '$." . static::sanitizeName($s_columnOld) . "')) = 'NULL', NULL, JSON_UNQUOTE(JSON_EXTRACT(data, '$." . static::sanitizeName($s_columnOld) . "'))) ";

            $newValue = sprintf($this->getSubfieldConverter($a_fieldUpdate['structureTo']), $oldValue);

            $s_setExpr .= ", '$." . static::sanitizeName($s_columnNew) . "', " . $newValue . " ";
		}

        $s_setExpr .= ')';

        if (count($a_remove) > 0) {
            $s_setExpr = 'JSON_REMOVE(' . $s_setExpr;

            foreach ($a_remove as $s_remove) {
                $s_setExpr .= ", '$." . $s_remove . "'";
            }

            $s_setExpr .= ')';
        }

        $s_query = "UPDATE ".$this->qs_table." SET data = " . $s_setExpr . $this->scopeWhere();

		$sth = $this->PDO->prepare($s_query);
		$sth->execute();

		if($sth->errorCode() !== '00000') {
			// @codeCoverageIgnoreStart
			throw new StorageException("Could not update JSON columns.");
			// @codeCoverageIgnoreEnd
		}
	}
	
	/**
	 * @inherit
	 */
	public function removeColumns($a_columns) {
        if (count($a_columns) === 0) {
            return;
        }

        $s_setExpr = 'JSON_REMOVE(data';

        foreach ($a_columns as $s_column) {
            $s_setExpr .= ", '$." . static::sanitizeName($s_column) . "'";
        }

        $s_setExpr .= ')';

        $s_query = "UPDATE ".$this->qs_table." SET data = " . $s_setExpr . $this->scopeWhere();

        $sth = $this->PDO->prepare($s_query);
        $sth->execute();

        if($sth->errorCode() !== '00000') {
            // @codeCoverageIgnoreStart
            throw new StorageException("Could not drop JSON columns.");
            // @codeCoverageIgnoreEnd
        }
	}
	
	/**
	 * @inherit
	 */
	public function next() : int {
        $sth = $this->PDO->prepare("SHOW TABLE STATUS LIKE " . $this->PDO->quote($this->s_table));
        $sth->execute();
		$metaValue = $sth->fetch(PDO::FETCH_ASSOC)['Auto_increment'];

		$sth = $this->PDO->prepare("SELECT COALESCE(MAX(entry_id), 0) + 1 AS `next` FROM " . $this->qs_table);
		$sth->execute();
		return max($metaValue, $sth->fetch(PDO::FETCH_ASSOC)['next']);
	}

    /**
     * @inherit
     */
    public function getColumns() : array {
        $sth = $this->PDO->prepare("SELECT * FROM ".$this->qs_table . $this->scopeWhere() . " LIMIT 1");
        $sth->execute();
        $a_rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (count($a_rows) === 0) {
            throw new LogicException('Cannot get JSON columns when there is no data');
        }

        return array_keys($this->columnsToFlat($a_rows[0]));
    }

    /**
     * @inherit
     */
    public function getTableName(): string
    {
        return $this->s_table;
    }

	/**
	 * @inherit
	 */
	public static function quoteIdentifier($s_name) {
		return '`'.static::sanitizeName($s_name).'`';
	}

    /**
     * Build a condition string x AND y AND x
     */
    private function scopeCondition(): string
    {
        $scope = [];
        foreach ($this->scope as $column => $value) {
            $scope[] = $column . ' = ' . ((int) $value);
        }
        return implode(' AND ', $scope);
    }

    /**
     * Build a condition string including leading keyword; e.g. WHERE x AND y or AND x AND y
     */
    public function scopeWhere(string $keyword = 'WHERE'): string
    {
        $condition = $this->scopeCondition();
        return $condition ? ' ' . $keyword . ' ' . $condition . ' ' : '';
    }

    public function deleteStorage(): bool
    {
        $sth = $this->PDO->prepare("DELETE FROM ".$this->qs_table." " . $this->scopeWhere());
        $sth->execute();
        return $sth->errorCode() === '00000';
    }

    public function count(): int
    {
        $sth = $this->PDO->prepare("
			SELECT COUNT(entry_id) AS num
			FROM ".$this->qs_table."
			" . $this->scopeWhere(). "
		");
        $sth->execute();
        $a_rows = $sth->fetchAll(PDO::FETCH_NUM);
        return (int)$a_rows[0][0];
    }

    public function list(): array
    {
        $sth = $this->PDO->prepare("
			SELECT entry_id
			FROM ".$this->qs_table."
			" . $this->scopeWhere(). "
			ORDER BY entry_id ASC
		");
        $sth->execute();
        $a_rows = $sth->fetchAll(PDO::FETCH_NUM);
        return array_column($a_rows, 0);
    }

    /**
     * @inherit
     */
    private function queryAll(int $i_offset, int $i_num) : \PDOStatement {
        if($i_num <= 0) {
            $i_num = PHP_INT_MAX;
        }
        $sth = $this->PDO->prepare("
			SELECT *
			FROM ".$this->qs_table."
			" . $this->scopeWhere(). "
			ORDER BY entry_id ASC
			LIMIT ".$i_num."
			OFFSET ".$i_offset."
		");
        $sth->execute();
        return $sth;
    }

    private function flatToColumns(array $flat): array
    {
        $columns = $this->scope;
        if (isset($flat['_uuid'])) {
            $columns['uuid'] = $flat['_uuid'];
        }
        $columns['data'] = json_encode(array_diff_key($flat, ['_entry_id' => 1, '_uuid' => 1]));

        if ($columns['data'] === '[]') {
            $columns['data'] = '{}';
        }

        return $columns;
    }

    private function columnsToFlat(array $columns): array
    {
        $flat = json_decode($columns['data'], true);
        if (!is_array($flat)) {
            throw new RuntimeException();
        }
        $flat['_entry_id'] = $columns['entry_id'];
        $flat['_uuid'] = $columns['uuid'];
        return $flat;
    }

    public function table(int $i_offset = 0, int $i_num = -1): array
    {
        $sth = $this->queryAll($i_offset, $i_num);
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows as $i => $row) {
            $rows[$i] = $this->columnsToFlat($row);
        }
        return $rows;
    }

    public function generator(int $i_offset = 0, int $i_num = -1): iterable
    {
        $sth = $this->queryAll($i_offset, $i_num);

        while($a_row = $sth->fetch(PDO::FETCH_ASSOC)) {
            yield $this->columnsToFlat($a_row);
        }
    }

    public function insert(array $a_data): int
    {
        unset($a_data['_entry_id']);
        $a_data['_uuid'] = $a_data['_uuid'] ?? \Reef\unique_id();

        $a_keys = $a_values = [];

        foreach ($this->flatToColumns($a_data) as $s_key => $s_value) {
            $a_keys[] = static::quoteIdentifier($s_key);
            $a_values[] = $s_value;
        }

        $s_keys = implode(', ', $a_keys);
        $s_valueQs = str_repeat('?,', count($a_values)-1).'?';

        $sth = $this->PDO->prepare("
			INSERT INTO ".$this->qs_table."
			(".$s_keys.")
			VALUES (".$s_valueQs.")
		");
        $sth->execute($a_values);
        return (int)$this->PDO->lastInsertId();
    }

    public function update(int $i_entryId, array $a_data)
    {
        unset($a_data['_entry_id']);
        unset($a_data['_uuid']);

        $a_sets = $a_values = [];

        foreach($this->flatToColumns($a_data) as $s_key => $s_value) {
            $a_sets[] = static::quoteIdentifier($s_key) . " = ? ";
            $a_values[] = $s_value;
        }

        $a_values[] = $i_entryId;

        if(empty($a_sets)) {
            return 1;
        }

        $s_sets = implode(', ', $a_sets);

        $sth = $this->PDO->prepare("
			UPDATE ".$this->qs_table."
			SET ".$s_sets."
			WHERE entry_id = ? " . $this->scopeWhere('AND') . "
		");
        $sth->execute($a_values);
        return $sth->rowCount();
    }

    public function delete(int $i_entryId)
    {
        $sth = $this->PDO->prepare("
			DELETE FROM ".$this->qs_table."
			WHERE entry_id = ? " . $this->scopeWhere('AND') . "
		");
        $sth->execute([$i_entryId]);
        return $sth->rowCount();
    }

    public function get(int $i_entryId): array
    {
        $sth = $this->PDO->prepare("
			SELECT * FROM ".$this->qs_table."
			WHERE entry_id = ? " . $this->scopeWhere('AND') . "
		");
        $sth->execute([$i_entryId]);
        $a_result = $sth->fetchAll(PDO::FETCH_ASSOC);

        if(count($a_result) == 0) {
            throw new StorageException('Could not find entry '.$i_entryId);
        }

        $a_result = $a_result[0];

        return $this->columnsToFlat($a_result);
    }

    public function getOrNull(int $i_entryId): ?array
    {
        try {
            return $this->get($i_entryId);
        }
        catch(StorageException $e) {
            return null;
        }
    }

    public function getByUUID(string $s_uuid): array
    {
        $sth = $this->PDO->prepare("
			SELECT * FROM ".$this->qs_table."
			WHERE uuid = ? " . $this->scopeWhere('AND') . "
		");
        $sth->execute([$s_uuid]);
        $a_result = $sth->fetchAll(PDO::FETCH_ASSOC);

        if(count($a_result) == 0) {
            throw new StorageException('Could not find entry with uuid '.$s_uuid);
        }

        $a_result = $a_result[0];

        return $this->columnsToFlat($a_result);
    }

    public function getByUUIDOrNull(string $s_uuid): ?array
    {
        try {
            return $this->getByUUID($s_uuid);
        }
        catch(StorageException $e) {
            return null;
        }
    }

    public function exists(int $i_entryId): bool
    {
        $sth = $this->PDO->prepare("
			SELECT entry_id FROM ".$this->qs_table."
			WHERE entry_id = ? " . $this->scopeWhere('AND') . "
		");
        $sth->execute([$i_entryId]);
        $a_result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return count($a_result) > 0;
    }
}
