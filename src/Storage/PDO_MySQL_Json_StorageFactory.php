<?php

namespace Reef\Storage;

use \PDO;
use \Reef\Exception\InvalidArgumentException;
use Reef\Exception\LogicException;
use Reef\Form\AbstractStorableForm;
use Reef\Form\StoredForm;

/**
 * Factory for PDO_MySQL_DDL_Storage objects
 */
class PDO_MySQL_Json_StorageFactory extends PDO_StorageFactory {
    /**
     * @var string
     */
	private $formTableName;

    /**
     * @var string
     */
    private $submissionTableName;

    /**
     * The form storage instance
     * @type Storage
     */
    private $FormStorage;

    /**
     * The submission storage instances, indexed by storage name
     * @type Storage[]
     */
    private $a_submissionStorages = [];

	/**
	 * @inherit
	 */
	public function __construct(PDO $PDO, string $formTableName, string $submissionTableName) {
		parent::__construct($PDO);

        $this->formTableName = $formTableName;
        $this->submissionTableName = $submissionTableName;
		
		if($PDO->query("SELECT CHARSET('')")->fetchColumn() != 'utf8mb4') {
			// @codeCoverageIgnoreStart
			throw new InvalidArgumentException("Please use utf8mb4 as MySQL charset, e.g. by setting `;charset=utf8mb4` in the PDO constructor or by using `SET NAMES utf8mb4;`.");
			// @codeCoverageIgnoreEnd
		}
	}
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'pdo_mysql_json';
	}
	
	/**
	 * @inherit
	 */
	protected function getStorageClass() {
		return '\\Reef\\Storage\\PDO_MySQL_Json_Storage';
	}
	
	/**
	 * @inherit
	 */
	protected function getPDODriverName() {
		return 'mysql';
	}

    public function getFormStorage(): Storage
    {
        if (!$this->FormStorage) {
            $this->FormStorage = new PDO_MySQL_Json_Storage($this, $this->getPDO(), $this->formTableName);
        }

        return $this->FormStorage;
    }

    public function hasSubmissionStorage(AbstractStorableForm $Form): bool
    {
        return true;
    }

    public function createSubmissionStorage(StoredForm $Form): Storage
    {
        return $this->getSubmissionStorage($Form);
    }

    public function getSubmissionStorage(StoredForm $Form): Storage
    {
        if ($Form->getFormId() === null) {
            throw new LogicException('Cannot get submission storage for unsaved form');
        }

        if (!isset($this->a_submissionStorages[$Form->getFormId()])) {
            $this->a_submissionStorages[$Form->getFormId()] =
                new PDO_MySQL_Json_Storage($this, $this->getPDO(), $this->submissionTableName, [
                    'form_id' => $Form->getFormId(),
                ]);
        }

        return $this->a_submissionStorages[$Form->getFormId()];
    }

    public function deleteSubmissionStorage(StoredForm $Form): void
    {
        $this->getSubmissionStorage($Form)->deleteStorage();
    }

    public function deleteSubmissionStorageIfExists(StoredForm $Form): void
    {
        $this->deleteSubmissionStorage($Form);
    }

    public function recreateTables(): void
    {
        $this->getPDO()->prepare("DROP TABLE IF EXISTS " . $this->submissionTableName)->execute();
        $this->getPDO()->prepare("DROP TABLE IF EXISTS " . $this->formTableName)->execute();

        $this->getPDO()->prepare("CREATE TABLE " . $this->formTableName . " (
            entry_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            uuid BINARY(32),
            data JSON,
            PRIMARY KEY (entry_id),
            UNIQUE INDEX uuid (uuid)
        )")->execute();

        $this->getPDO()->prepare("CREATE TABLE " . $this->submissionTableName . " (
            entry_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            uuid BINARY(32),
            form_id INT UNSIGNED NOT NULL,
            data JSON,
            PRIMARY KEY (entry_id),
            UNIQUE INDEX uuid (uuid),
            FOREIGN KEY (form_id) REFERENCES " . $this->formTableName . "(entry_id)
        )")->execute();
    }
}
