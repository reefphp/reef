<?php

namespace Reef\Storage;

use \PDO;
use \Reef\Exception\RuntimeException;
use \Reef\Exception\InvalidArgumentException;
use Reef\Reef;

/**
 * Factory for PDO_Storage objects
 */
abstract class PDO_StorageFactory implements StorageFactory {
    /**
     * The PDO instance
     * @type PDO
     */
    private $PDO;

    /**
     * The Reef object this Form belongs to
     *
     * @var Reef
     */
    protected $Reef;

    /**
     * List of transaction savepoints
     * @type string[]
     */
    private $a_savepoints = [];

    /**
     * Savepoint counter used to create unique savepoints
     * @type int
     */
    private $i_savepointCounter = 0;

    /**
     * Number of nested withEnsuredTransaction() calls
     * @type int
     */
    private $i_ensuredTransactionCount = 0;

    /**
     * Constructor
     * @param PDO $PDO The PDO instance to use
     */
    public function __construct(PDO $PDO) {
        $this->PDO = $PDO;

        if($this->PDO->getAttribute(PDO::ATTR_ERRMODE) != PDO::ERRMODE_EXCEPTION) {
            // @codeCoverageIgnoreStart
            throw new InvalidArgumentException("PDO attribute PDO::ATTR_ERRMODE should be set to PDO::ERRMODE_EXCEPTION");
            // @codeCoverageIgnoreEnd
        }

        if($this->PDO->getAttribute(PDO::ATTR_DRIVER_NAME) != $this->getPDODriverName()) {
            // @codeCoverageIgnoreStart
            throw new InvalidArgumentException("PDO storage ".$this->getPDODriverName()." does not support PDO driver ".$this->PDO->getAttribute(PDO::ATTR_DRIVER_NAME)." ");
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * Set the Reef object
     * @param Reef $Reef The Reef we are currently working in
     */
    public function setReef(Reef $Reef): void {
        $this->Reef = $Reef;
    }

    protected function getPDO(): PDO
    {
        return $this->PDO;
    }

    /**
     * Return the storage class path
     * @return string
     */
    abstract protected function getStorageClass();

    /**
     * Return the PDO driver name
     * @return string
     */
    abstract protected function getPDODriverName();

    /**
     * Execute a closure with the knowledge a transaction has already started on the PDO instance
     */
    public function withEnsuredTransaction(\Closure $fn_callback)
    {
        try {
            $this->i_ensuredTransactionCount++;
            return $fn_callback();
        } finally {
            $this->i_ensuredTransactionCount--;
        }
    }

    /**
     * Determine whether currently a transaction is active
     * @return bool
     */
    public function inTransaction() {
        return (count($this->a_savepoints) > 0);
    }

    /**
     * Utility function for applying modifications in the database, ensured
     * that these modifications are all performed in a single transaction.
     * @param callable $fn_callback Callback performing the desired modifications
     * @return mixed The result of $fn_callback
     */
    public function ensureTransaction($fn_callback) {

        $s_storageClass = $this->getStorageClass();
        if(count($this->a_savepoints) == 0 && $this->i_ensuredTransactionCount === 0) {
            $s_storageClass::startTransaction($this->PDO);
        }

        $s_savepoint = 'savepoint_'.($this->i_savepointCounter++);
        $s_storageClass::newSavepoint($this->PDO, $s_savepoint);
        $this->a_savepoints[] = $s_savepoint;

        try {
            $m_return = $fn_callback();
        }
        catch(\Exception $e) {
            if(end($this->a_savepoints) != $s_savepoint) {
                // @codeCoverageIgnoreStart
                $s_storageClass::rollbackTransaction($this->PDO);
                throw new RuntimeException("Corrupt last savepoint", 0, $e);
                // @codeCoverageIgnoreEnd
            }

            $s_storageClass::rollbackToSavepoint($this->PDO, $s_savepoint);
            throw $e;
        }
        finally {
            array_pop($this->a_savepoints);
        }

        if(count($this->a_savepoints) == 0 && $this->i_ensuredTransactionCount === 0) {
            $s_storageClass::commitTransaction($this->PDO);
        }

        return $m_return;
    }

}
