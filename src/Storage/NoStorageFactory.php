<?php

namespace Reef\Storage;

use Reef\Form\AbstractStorableForm;
use Reef\Form\StoredForm;
use Reef\Reef;

/**
 * Factory for NoStorage objects
 */
class NoStorageFactory implements StorageFactory {
    /**
     * The Reef object this Form belongs to
     *
     * @var Reef
     */
    protected $Reef;

    /**
	 * @inherit
	 */
	public static function getName() : string {
		return 'nostorage';
	}
	
	/**
	 * Constructor
	 */
	public function __construct() {
	}

    /**
     * Set the Reef object
     * @param Reef $Reef The Reef we are currently working in
     */
    public function setReef(Reef $Reef): void {
        $this->Reef = $Reef;
    }

    public function getFormStorage(): Storage
    {
        return new NoStorage();
    }

    public function hasSubmissionStorage(AbstractStorableForm $Form): bool
    {
        return false;
    }

    public function createSubmissionStorage(StoredForm $Form): Storage
    {
        return new NoStorage();
    }

    public function getSubmissionStorage(StoredForm $Form): Storage
    {
        return new NoStorage();
    }

    public function deleteSubmissionStorage(StoredForm $Form): void
    {
    }

    public function deleteSubmissionStorageIfExists(StoredForm $Form): void
    {
    }
}
