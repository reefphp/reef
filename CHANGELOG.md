## Unreleased

## 0.4.0 (2023-02-04)

- Added `PDO_MySQL_Json_Storage`
  - Renamed `PDO_MySQL_Storage` to `PDO_MySQL_DDL_Storage`
  - Renamed `PDO_SQLite_Storage` to `PDO_SQLite_DDL_Storage`
  - Changed `StorageFactory` interface definition
- Added `PDO_StorageFactory::withEnsuredTransaction()`

## 0.3.7 (2022-12-20)

- Update psr/simple-cache dependency

## 0.3.6 (2022-12-19)

- Update to support php 8.1 & php 8.2
- Add support for Bootstrap 5
- Don't present an immediate required error when toggling open a field

## 0.3.5 (2021-12-23)

- Update to support php 8.0

## 0.3.4 (2020-04-12)

- Fix psr/simple-cache dependency

## 0.3.3 (2020-02-16)

- Update Symfony dependencies
- Add support for running and testing Reef in Docker

## 0.3.2 (2019-01-18)

- Improve locale coalescence

## 0.3.1 (2018-12-24)

- Solve bug where the javascript implementation of number comparison in conditions was performed incorrectly (lexically instead of numerically) (#17)

## 0.3.0 (2018-11-15)

- Add submit_wrapper option to JS builder

## 0.2.0 (2018-11-08)

### Second minor release

- Second development release of Reef
- All essential functionality is present

## 0.1.0 (2018-09-11)

### Initial release

- First development release of Reef
