<?php

session_start();

require_once(__DIR__ . '/../vendor/autoload.php');

define('TEST_TMP_DIR', getenv('IN_DOCKER') ? '/var/tmp/test' :  __DIR__ . '/../var/tmp/test');

$_reef_PDO = new \PDO("sqlite::memory:");
$_reef_PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

$_reef_getStorageFactory = function () use ($_reef_PDO) {
    return \Reef\Storage\PDO_DDL_StorageFactory::createDdlFactory($_reef_PDO);
};

$_reef_setup = new \Reef\ReefSetup(
    $_reef_getStorageFactory(),
	new \Reef\Layout\bootstrap4\bootstrap4(),
	new \Reef\Session\TmpSession()
);

$_reef_reef = new \Reef\Reef(
	$_reef_setup,
	[
	]
);
