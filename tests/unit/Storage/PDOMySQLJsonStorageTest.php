<?php

namespace ReefTests\unit\Storage;

use \PDOException;
use Reef\Storage\PDO_MySQL_Json_Storage;

final class PDOMySQLJsonStorageTest extends PDOStorageTestCase {

    const DB_NAME = 'reef_test';
    const DB_USER = 'reef_test';
    const DB_PASS = 'reef_test';

    private static $PDOException = null;

    protected static $s_storageName = 'reef_test_form';

    public static function setUpBeforeClass(): void {
        try {
            if(getenv('IN_SCRUTINIZER_CI')) {
                static::$PDO = new \PDO("mysql:dbname=".static::DB_NAME.";host=127.0.0.1;charset=utf8mb4", "root", "");
            }
            else {
                $DB_HOST = getenv('IN_GITLAB_CI') ? 'mysql' : (getenv('IN_DOCKER') ? 'mysql' : '127.0.0.1');
                static::$PDO = new \PDO("mysql:dbname=".static::DB_NAME.";host=".$DB_HOST.";charset=utf8mb4", static::DB_USER, static::DB_PASS);
            }
            static::$PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            static::$PDO = null;
            static::$PDOException = $e;
        }
    }

    public function setUp(): void {
        if(empty(static::$PDO)) {
            $this->markTestSkipped(
                'Could not connect to MySQL database ('.static::$PDOException->getMessage().')'
            );
        }
    }

    public function testCanBeCreated(): void {
        static::$PDO_Factory = new \Reef\Storage\PDO_MySQL_Json_StorageFactory(
            self::$PDO,
            'reef_test_form',
            'reef_test_submission'
        );

        $this->assertInstanceOf(
            PDO_MySQL_Json_Storage::class,
            static::$Storage = static::$PDO_Factory->getFormStorage()
        );

        static::$PDO_Factory->recreateTables();
    }

    public function testCanInsertEmptyData(): void {
        self::$PDO->beginTransaction();

        try {
            $a_data = [];

            $i_entryId = static::$Storage->insert($a_data);

            $a_data2 = static::$Storage->get($i_entryId);
            $this->assertIsArray($a_data2);

            $this->assertSame($a_data, \Reef\array_subset($a_data2, ['value', 'number']));

            $sth = self::$PDO->prepare("SELECT * FROM reef_test_form WHERE entry_id = " . ((int)$i_entryId) . " ");
            $sth->execute();
            $a_rows = $sth->fetchAll(\PDO::FETCH_ASSOC);
            $this->assertSame('{}', $a_rows[0]['data']);
        } finally {
            self::$PDO->rollback();
        }
    }
}
