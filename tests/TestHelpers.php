<?php

namespace ReefTests;

trait TestHelpers
{
    private function assertSameArrayUnordered(array $expected, array $actual): void
    {
        $this->assertCount(count($expected), $actual);
        $this->assertEmpty(array_diff($expected, $actual));
    }
}
